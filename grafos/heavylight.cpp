#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;
#define dprint(v) cerr << #v"=" << v << endl //;)
#define forr(i,a,b) for(int i=(a); i<(b); i++)
#define forn(i,n) forr(i,0,n)
#define dforn(i,n) for(int i=n-1; i>=0; i--)
#define forall(it,v) for(typeof(v.begin()) it=v.begin();it!=v.end();++it)
#define sz(c) ((int)c.size())
#define zero(v) memset(v, 0, sizeof(v))
typedef long long ll;
typedef pair<int,int> ii;
const int MAXN=100100;
vector<int> G[MAXN];
int n;

int treesz[MAXN];//cantidad de nodos en el subarbol del nodo v
int dad[MAXN];//dad[v]=padre del nodo v
void dfs1(int v, int p=-1){//pre-dfs
	dad[v]=p;
	treesz[v]=1;
	forall(it, G[v]) if(*it!=p){
		dfs1(*it, v);
		treesz[v]+=treesz[*it];
	}
}
/*uso:
 *rmq.init(n)
 *dfs1(inicial);
 *heavylight(inicial);
 *dfs(); (o poner la llamada adentro de build)
 *rmq[pos[v]] = peso entre dad[v] y v para todo v (aqui se hace como parte de heavylight, si la lista de adjacencia tiene pares(hijo,peso);
 *build(inicial);
 *rmq.updall(); */
int pos[MAXN], q;//pos[v]=posicion del nodo v en el recorrido de la dfs
//Las cadenas aparecen continuas en el recorrido!
int cantcad;
int homecad[MAXN];//dada una cadena devuelve su nodo inicial
int cad[MAXN];//cad[v]=cadena a la que pertenece el nodo
void heavylight(int v, int cur=-1, int peso = -1){
	if(cur==-1) homecad[cur=cantcad++]=v;
	pos[v]=q++;
	rmq[pos[v]]=peso;
	cad[v]=cur;
	int mx=-1;
	for(int i = 0; i < G[v].size(); i++) if(G[v][i].first!=dad[v])
		if(mx==-1 || treesz[G[v][mx].first]<treesz[G[v][i].first]) mx=i;
	if(mx!=-1) heavylight(G[v][mx].first, cur, G[v][mx].second);
	for(int i = 0; i < G[v].size(); i++) if(i!=mx && G[v][i].first!=dad[v])
		heavylight(G[v][i].first, -1, G[v][i].second);
}
// r = max(query(lca(v,u), v), query(lca(u,v),u));
int query(int an, int v){//O(logn)
	if(an == v) return -1;
	if(cad[an]==cad[v] && homecad[cad[an]] == an) return rmq.get(pos[an]+1, pos[v]+1);
	if(cad[an]==cad[v]) return rmq.get(pos[an]+1, pos[v]+1);
	return max(query(an, dad[homecad[cad[v]]]),
			   rmq.get(pos[homecad[cad[v]]], pos[v]+1));
}
